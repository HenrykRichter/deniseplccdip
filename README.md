# DenisePLCCDIP

This project contains a simple adapter PCB for mounting a PLCC Denise chip in Amiga 500/2000/3000 computers.
The regular DIP version of the Denise 8373 has become quite rare in recent years. Sometimes, the A600 Denise
is easier to obtain. This adapter is supposed to convert the pinout of the 8373R4PL Denise to DIP-48. See below for pictures.

## Building Notes

I would suggest to solder the pin strips first. Once done, the best course of action is to shorten the
pins who sit under the PLCC socket. Mount the PLCC socket next and carefully solder its pins at the underside of the board.

## Parts list
- PCB
- 2 pin strips, 24 pins each
- 1 THT PLCC socket, 52 pins (e.g. 575-945224 at Mouser)

## Pictures

![Top Side](https://gitlab.com/HenrykRichter/deniseplccdip/raw/master/Pics/Denise_Adapter_Top.jpg)

![Bottom Side](https://gitlab.com/HenrykRichter/deniseplccdip/raw/master/Pics/Denise_Adapter_Bottom.jpg)

![In A500 Board](https://gitlab.com/HenrykRichter/deniseplccdip/raw/master/Pics/Denise_Adapter_Mounted.jpg)


